angular.module('starter.services', [])

.service('LoginService', function($resource,SERVER_PATH) {
  return $resource('action:', {},
      {
        authenticate: {
          method: 'POST',
          url:SERVER_PATH.path+'/login/authenticate'
        }
      }
    );
})

.service('ProfileService', function($resource,SERVER_PATH) {
  return $resource('action1:', {},
      {
        getCurrentUser: {
          method: 'GET',
          url:SERVER_PATH.path+'/users/getCurrentUser'
        }

      }
    );
})


.service('DeviceService', function($resource,SERVER_PATH) {
  return $resource('action:', {},
      {
        addNewDevice: {
          method: 'POST',
          url:SERVER_PATH.path+'/devices/addNewDevice'
        },

        deleteDevice: {
          method: 'DELETE',
          url:SERVER_PATH.path+'/devices/deleteDevice/'
        }

      }
    );
})

.service('PopupService', function($ionicPopup) {
   
         showPopupConfirmation=function(message,functiontoExecute){
           var confirmPopup = $ionicPopup.confirm({
           title: 'Confirmation',
           template: message
           });

           confirmPopup.then(function(result) {
           if(result) {
             functiontoExecute;
           }});

         }
          
         var popup={};
         popup.showPopup=function(title, message, goTo){
          var popupAlert = $ionicPopup.alert({
           title: title,
           template: message
          });

          popupAlert.then(function(result) {
             if(goTo!==''){
             $state.go(goTo);
            }
          });
          return popup;
         }
         

})



;
